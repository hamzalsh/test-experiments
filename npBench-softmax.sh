#!/bin/bash

raw_dest="${PWD}/results/softmax.txt"

# Run and save raw results
(cd npbench/ ; numactl -N 0 python3 run_benchmark.py -b softmax -f numpy -r 1 -p L -v False -w False > $raw_dest 2>&1)
run_status=$?

benchmark_result_dest=results/results.txt 

# Output filtering includes: finding specific line, extracting the value (follows "="), and removing white spaces
cat $raw_dest | grep "median" | awk -F "-" '{print $3}' | sed "s/ median://g" | sed "s/ms//g" | sed 's/^[ \t]*//' >> $benchmark_result_dest

exit $run_status
